package com.univer;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpConnection;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;


public class ProductActivity extends Activity {

    private final static String TAG = "ProductActivity";
    private final static int REQUEST_TAKE_PHOTO = 1;

    private InventoryStorage db;
    private ImageView imageView;
    private Product product;
    private String serverUrl;

    private class RequestTask extends AsyncTask<Integer, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(Integer... params) {
            Log.d("RequestTask", "doInBackground");
            Bitmap bitmap = null;
            try {
                bitmap = BitmapFactory.decodeStream(
                    new URL(serverUrl + "static/"+ params[0] +".jpg").openStream()
                );

            } catch (MalformedURLException e) {
                Log.e(TAG, "URL parsing was failed");
            } catch (IOException e) {
                Log.d(TAG, "URL does not exists");
            }

            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            //dialogProgress.dismiss();
            if (bitmap == null) {
                bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.nophoto);
            }
            imageView.setImageBitmap(bitmap);
            super.onPostExecute(bitmap);
        }

        @Override
        protected void onPreExecute() {
            //productsDatabase.clear();
            /*
            dialogProgress = new ProgressDialog(StartActivity.this);
            dialogProgress.setMessage(StartActivity.this.getString(R.string.title_loading_products));
            dialogProgress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            dialogProgress.setProgress(0);
            dialogProgress.setCancelable(true);
            dialogProgress.show();
            */
            super.onPreExecute();
        }
    }

    private class RequestUploadTask extends AsyncTask<String, Void, Integer> {
        private static final String serverUrl = "http://univer1.entrydns.org:58888/product/";

        @Override
        protected Integer doInBackground(String... params) {
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl + params[1] + "/");

            try {

                MultipartEntity entity = new MultipartEntity();
                entity.addPart("id", new StringBody(params[1]));
                entity.addPart("picture", new FileBody(new File(params[0])));
                post.setEntity(entity);
                HttpResponse response = client.execute(post);
                return response.getStatusLine().getStatusCode();

            } catch (Exception e) {

            }

            return 0;
        }

        @Override
        protected void onPostExecute(Integer code) {
            int sid = (code == 200) ? R.string.upload_succes : R.string.upload_error;
            Toast.makeText(getApplicationContext(), getString(sid), Toast.LENGTH_SHORT).show();
            super.onPostExecute(code);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        int id = getIntent().getIntExtra("productId", 0);

        imageView = (ImageView) findViewById(R.id.imageView);

        new RequestTask().execute(id);

        db = new InventoryStorage(this);
        serverUrl = "http://" + db.getSettingsValue("server") + ":58888/";

        ActionBar actionBar = getActionBar();
        if (actionBar != null)
            actionBar.setDisplayHomeAsUpEnabled(true);

        product = db.getProduct(id);
        if (product == null)
            Log.e(TAG, "Product not found: "+ id);

        TextView textName = (TextView) findViewById(R.id.name);
        textName.setText(product.name());
    }

    @Override
    protected void onDestroy() {
        db.close();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_product, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;

            case R.id.action_take_photo:
                cameraOpen();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);
    }

    public void cameraOpen() {
        Intent intent = new Intent(ProductActivity.this, CameraActivity.class);
        intent.putExtra("id", product.id());
        intent.putExtra("name", product.name());
        startActivityForResult(intent, REQUEST_TAKE_PHOTO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            String photoPath = data.getStringExtra("photoPath");
            Log.d(TAG, "Photo: "+ photoPath);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(data.getStringExtra("photoPath"), options);

            options.inJustDecodeBounds = false;
            options.inSampleSize = (options.outHeight / imageView.getHeight());
            imageView.setImageBitmap(BitmapFactory.decodeFile(photoPath, options));

            new RequestUploadTask().execute(photoPath, String.valueOf(product.id()));

        }
    }

}

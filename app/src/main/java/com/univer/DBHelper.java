package com.univer;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper {

    public static final String TABLE_DOCUMENT = "document";
    public static final String TABLE_PRODUCT = "product";
    public static final String TABLE_SETTINGS = "settings";
    public static final String TABLE_INVENTORY = "inventory";
    public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public DBHelper(Context context) {
        super(context, "univer.db", null, 5);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d("DB", "onCreate");

        db.execSQL("CREATE TABLE "+ TABLE_DOCUMENT +" ("
                        + "id INTEGER PRIMARY KEY AUTOINCREMENT,"
                        + "date DATETIME DEFAULT CURRENT_TIMESTAMP,"
                        + "external_id INTEGER"
                        + ")"
        );

        db.execSQL("CREATE TABLE "+ TABLE_PRODUCT +" ("
                        + "id INTEGER PRIMARY KEY AUTOINCREMENT,"
                        + "name VARCHAR(50),"
                        + "article VARCHAR(30),"
                        + "barcode VARCHAR(30),"
                        + "stock INTEGER,"
                        + "price REAL"
                        + ")"
        );

        db.execSQL("CREATE TABLE "+ TABLE_SETTINGS +" ("
                        + "name VARCHAR(50) UNIQUE,"
                        + "value TEXT"
                        + ")"
        );

        db.execSQL("CREATE TABLE "+ TABLE_INVENTORY +" ("
                        + "id INTEGER UNIQUE,"
                        + "document INTEGER,"
                        + "product INTEGER,"
                        + "count INTEGER,"
                        + "position INTEGER,"
                        + "UNIQUE(document, product)"
                        + ")"
        );

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d("DB", "onUpgrade "+ newVersion);
        if (oldVersion != newVersion) {
            db.execSQL("DROP TABLE " + TABLE_DOCUMENT);
            db.execSQL("DROP TABLE " + TABLE_PRODUCT);
            db.execSQL("DROP TABLE " + TABLE_SETTINGS);
            db.execSQL("DROP TABLE " + TABLE_INVENTORY);
            onCreate(db);
        }
    }

}
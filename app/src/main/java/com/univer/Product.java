package com.univer;

public class Product {

    public static final String PRODUCT_ID = "id";
    public static final String PRODUCT_NAME = "name";
    public static final String PRODUCT_ARTICLE = "article";
    public static final String PRODUCT_COUNT = "count";
    public static final String PRODUCT_STOCK = "stock";
    public static final String PRODUCT_PRICE = "price";
    public static final String PRODUCT_BARCODE = "barcode";

    private int id;
    private int count;
    private int stock;
    private double price;
    private String name;
    private String article;
    private String barcode;

    public Product() {}

    public Product(Integer id, String name, String article, int count, int stock, double price, String barcode) {
        this.id = id;
        this.name = name;
        this.article = article;
        this.count = count;
        this.stock = stock;
        this.price = price;
        this.barcode = barcode;
    }

    public static Product createEmpty(int id, int count, String barcode) {
        return new Product(id, "", "", count, 0, 0.0, barcode);
    }

    public String toString() {
        return "Product #"+ id() + " " + name();
    }

    public int id() {
        return this.id;
    }

    public String name() {
        return this.name;
    }

    public String article() {
        return this.article;
    }

    public int count() {
        return this.count;
    }

    public int stock() {
        return this.stock;
    }

    public double price() {
        return this.price;
    }

    public String barcode() {
        return this.barcode;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

}

package com.univer;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.RadioGroup;
import android.widget.Toast;


public class SettingsActivity extends Activity implements RadioGroup.OnCheckedChangeListener {

    private InventoryStorage db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        db = new InventoryStorage(this);

        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radioGroupShop);
        radioGroup.setOnCheckedChangeListener(this);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

        String name = "server";
        String value = "";

        switch (checkedId) {
            case R.id.radioButton1:
                value = "192.168.0.1";
                break;

            case R.id.radioButton2:
                value = "192.168.2.1";
                break;

            case R.id.radioButton3:
                value = "192.168.3.1";
                break;
        }

        db.setSettingsValue(name, value);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);
    }


}

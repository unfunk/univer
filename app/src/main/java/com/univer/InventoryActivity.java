package com.univer;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.util.LinkedList;

public class InventoryActivity extends Activity implements
        AdapterView.OnItemClickListener,
        AdapterView.OnItemLongClickListener
{

    private InventoryList productList;
    private ProductsDatabase productDatabase;
    private ListView listView;
    private MediaPlayer mediaPlayer, mediaPlayerError;
    private Vibrator vibrator;
    private Dialog dialogSearch;
    private Dialog dialogCount;
    private ProductAdapter adapter;
    private BluetoothScanner scanner;

    private Document document;
    private InventoryStorage db;

    private String serverUrl;

    class RequestPostTask extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            LinkedList<NameValuePair> pairs = new LinkedList<>();
            for (Product product : productList.values()) {
                pairs.add(new BasicNameValuePair(String.valueOf(product.id()), String.valueOf(product.count())));
            }
            try {
                DefaultHttpClient hc = new DefaultHttpClient();
                HttpPost http = new HttpPost(serverUrl + "inventory/");
                http.setEntity(new UrlEncodedFormEntity(pairs));
                HttpResponse response = hc.execute(http);
                if (response.getStatusLine().getStatusCode() != 200) {
                    Log.d("UPLOAD", "Http error "+ response.getStatusLine().getStatusCode());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory);
        db = new InventoryStorage(this);

        document = db.getDocument(getIntent().getLongExtra("documentId", 0));
        setTitle(getString(R.string.title_inventory, document.id()));

        mediaPlayer = MediaPlayer.create(this, R.raw.beep);
        mediaPlayerError = MediaPlayer.create(this, R.raw.beep4);
        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        listView = (ListView) findViewById(R.id.listViewInventory);

        productList = db.getInventoryProducts(document);
        serverUrl = "http://" + db.getSettingsValue("server") + ":58888/";

        adapter = new ProductAdapter(this, productList);

        listView.setAdapter(adapter);

        scanner = new BluetoothScanner(this, "00:1C:97:90:2E:C0");
        scanner.setHandler(new Handler() {
            @Override
            public void handleMessage(Message msg) {
                addProduct((String) msg.obj);
            }
        });

        listView.setOnItemClickListener(this);
        listView.setOnItemLongClickListener(this);

        ActionBar actionBar = getActionBar();
        if (actionBar != null)
            actionBar.setDisplayHomeAsUpEnabled(true);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    protected void onDestroy() {
        db.close();
        scanner.disconnect();
        super.onDestroy();
    }

    //@Override
    public boolean onQueryTextChange(String text_new) {
        //Log.d("QUERY", "New text:"+ text_new);
        return true;
    }

    //@Override
    public boolean onQueryTextSubmit(String text) {
        searchId(text);
        return false;
    }

    public void onSelectSearch() {

        if (dialogSearch == null) {
            dialogSearch = new Dialog(this);
            dialogSearch.setContentView(R.layout.search);
            dialogSearch.setTitle(R.string.action_search);

            final EditText editSearch = (EditText) dialogSearch.findViewById(R.id.editSearch);
            editSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus)
                        dialogSearch.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                }
            });

            Button buttonClear = (Button) dialogSearch.findViewById(R.id.buttonClear);
            buttonClear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editSearch.getText().clear();
                }
            });

            Button buttonAdd = (Button) dialogSearch.findViewById(R.id.buttonAdd);
            buttonAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    searchId(editSearch.getText().toString());
                    editSearch.getText().clear();
                    dialogSearch.dismiss();
                }
            });

            Button buttonSearch = (Button) dialogSearch.findViewById(R.id.buttonSearch);
            buttonSearch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        int id = Integer.valueOf(editSearch.getText().toString());
                        int position = productList.indexOfKey(id);
                        productList.setCurrent(position);
                        Log.d("SEARCH", "Position:"+ position);
                        listView.smoothScrollToPosition(position);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }

                    editSearch.getText().clear();
                    dialogSearch.dismiss();
                }
            });

        }

        dialogSearch.show();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
        productList.setCurrent(position);

        if (dialogCount == null) {
            dialogCount = new Dialog(this);
            dialogCount.setContentView(R.layout.count);
            dialogCount.setTitle(getString(R.string.action_count, id));
        }

        final EditText editCount = (EditText) dialogCount.findViewById(R.id.editCount);
        editCount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    dialogCount.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }
        });

        Button buttonOk = (Button) dialogCount.findViewById(R.id.buttonOk);
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Product product = productList.at(position);

                try {
                    int count = Integer.valueOf(editCount.getText().toString());
                    product.setCount(count);
                    onSearchSuccess(product);

                } catch (NumberFormatException e) {
                    Log.d("COUNT", "NumberFormatException");
                }

                editCount.getText().clear();
                dialogCount.dismiss();
            }
        });

        Button buttonClear = (Button) dialogCount.findViewById(R.id.buttonClear);
        buttonClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editCount.getText().clear();
            }
        });

        TextView textName = (TextView) dialogCount.findViewById(R.id.name);
        Product product = productList.at(position);
        textName.setText(product.name());
        editCount.setText(String.valueOf(product.count()));
        editCount.setSelection(editCount.getText().length());

        dialogCount.show();
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(InventoryActivity.this, ProductActivity.class);
        Log.d("onItemLongClick", "id:" + id);
        intent.putExtra("productId", (int)id);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out);
        return false;
    }

    public void onSelectUpload() {
        new RequestPostTask().execute();
    }

    public void onSelectScanner() {
        scanner.connect();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_inventory, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_save:
                onSelectUpload();
                return true;

            case R.id.action_scanner:
                onSelectScanner();
                return true;

            case R.id.action_search:
                onSelectSearch();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        if (!scanner.isConnected()) {
            finish();
            overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);
            return;
        }

        AlertDialog.Builder quitDialog = new AlertDialog.Builder(InventoryActivity.this);

        quitDialog.setTitle(this.getString(R.string.title_confirm_exit));

        quitDialog.setPositiveButton(this.getString(R.string.button_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
                overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);
            }
        });

        quitDialog.setNegativeButton(this.getString(R.string.button_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) { }
        });

        quitDialog.show();
    }

    @Override
    public boolean onSearchRequested() {
        Log.d("SEARCH", "onSearchRequested");

        return false;
    }

    public String getBarcode(int id) {
        Product product = productList.get(id);

        if (product != null)
            return product.barcode();

        return db.getBarcode(id);
    }

    public void searchId(String query) {
        try {
            int id = Integer.valueOf(query);
            String barcode = getBarcode(id);
            if (barcode.length() > 0) {
                addProduct(barcode);
                return;
            }

        } catch (NumberFormatException e) {
            Log.d("SEARCH", "Invalid id");
        }

        onSearchError(query);
    }

    private void onSearchError(String value) {
        Toast.makeText(this, getString(R.string.code_not_found, value), Toast.LENGTH_SHORT).show();
        mediaPlayerError.start();
        //vibrator.vibrate(400);
    }

    private void onSearchSuccess(Product product) {
        adapter.notifyDataSetChanged();
        productList.setCurrent(productList.indexOfKey(product.id()));
        listView.setSelection(productList.getCurrent());
        mediaPlayer.start();
        db.saveProductInventory(document, product, productList.getCurrent());
    }

    public boolean addProduct(String barcode) {
        if (barcode.length() == 0) {
            onSearchError(barcode);
            return false;
        }

        Product product = productList.get(barcode);
        if (product == null) {
            product = db.getProduct(barcode);

            if (product == null) {
                Log.d("SEARCH", "Product not found");
                onSearchError(barcode);
                return false;
            }
        }

        productList.put(product);

        onSearchSuccess(product);

        return true;
    }

}

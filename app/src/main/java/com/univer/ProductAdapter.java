package com.univer;

import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ProductAdapter extends BaseAdapter {

    private InventoryList data;
    private final LayoutInflater inflater;

    public ProductAdapter(Context context, InventoryList data) {
        inflater = LayoutInflater.from(context);
        setData(data);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Product product = getItem(position);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.inventory_item, null);
        }

        int countColor = product.stock() != product.count() ? 0x80ff0000 : 0x80002d74;
        int highlightColor = data.getCurrent() == position ? 0x400082ff : 0x00000000;

        convertView.setBackgroundColor(highlightColor);

        ((TextView) convertView.findViewById(R.id.name)).setText(product.name());
        ((TextView) convertView.findViewById(R.id.article)).setText(product.article());
        ((TextView) convertView.findViewById(R.id.id)).setText(String.valueOf(product.id()));
        ((TextView) convertView.findViewById(R.id.stock)).setText(String.valueOf(product.stock()));
        ((TextView) convertView.findViewById(R.id.price)).setText(String.valueOf(product.price()));
        ((TextView) convertView.findViewById(R.id.barcode)).setText(product.barcode());

        TextView textCount = ((TextView) convertView.findViewById(R.id.count));
        textCount.setText(String.valueOf(product.count()));
        textCount.setBackgroundColor(countColor);
        return convertView;
    }

    public void setData(InventoryList data) {
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Product getItem(int position) {
        return data.at(position);
    }

    @Override
    public long getItemId(int position) {
        return data.keyAt(position);
    }
}

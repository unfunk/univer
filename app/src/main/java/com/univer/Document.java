package com.univer;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class Document {
    static final String PRODUCT_ID = "id";
    static final String PRODUCT_DATE = "date";

    private long id;
    private Date date;
    private long external_id;

    Document(long id, Date date, long external_id) {
        this.id = id;
        setDate(date);
        this.external_id = external_id;
    }

    Document(long id, Date date) {
        this.id = id;
        this.date = date;
        this.external_id = 0;
    }

    Document(long id, String date, long external_id) {
        SimpleDateFormat df = new SimpleDateFormat(DBHelper.DATE_FORMAT);
        try {
            this.date = df.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.id = id;
        this.external_id = id;
    }

    public long id() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setDate(Date date) {
        this.date = (date != null) ? date : new Date();
    }

    public Date date() {
        return this.date;
    }

    public void setExternalId(long id) {
        this.external_id = id;
    }

    public long externalId() {
        return this.external_id;
    }

}
package com.univer;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class DocumentAdapter extends BaseAdapter {

    public final static String DATE_FORMAT = "dd-MM-yyyy HH:mm";

    private ArrayList<Document> data;
    private final LayoutInflater inflater;
    private final SimpleDateFormat dateFormat;

    public DocumentAdapter(Context context, ArrayList<Document> data) {
        inflater = LayoutInflater.from(context);
        dateFormat = new SimpleDateFormat(DATE_FORMAT);
        setData(data);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Document document = getItem(position);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.document_item, null);
        }

        ((TextView) convertView.findViewById(R.id.title)).setText(
            inflater.getContext().getString(R.string.title_inventory, document.id())
        );
        ((TextView) convertView.findViewById(R.id.date)).setText(dateFormat.format(document.date()));

        return convertView;
    }

    public void setData(ArrayList<Document> data) {
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Document getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).id();
    }
}

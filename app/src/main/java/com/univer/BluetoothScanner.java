package com.univer;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.UUID;

public class BluetoothScanner {

    public static final String UUID_SPP = "00001101-0000-1000-8000-00805F9B34FB";

    BluetoothAdapter adapter;
    BluetoothDevice device;
    BluetoothSocket socket;
    BluetoothScannerThread thread;
    Handler dataHandler;
    Handler toastHandler;
    Context context;

    public BluetoothScanner(Context ctx, String mac) {
        context = ctx;
        adapter = BluetoothAdapter.getDefaultAdapter();
        if (!adapter.isEnabled()) {
            adapter.enable();
        }

        device = adapter.getRemoteDevice(mac);

        toastHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                Toast.makeText(context, (String) msg.obj, Toast.LENGTH_SHORT).show();
            }
        };

    }

    public static void sendMessage(Handler handler, Object obj) {
        Message msg = new Message();
        msg.obj = obj;
        handler.sendMessage(msg);
    }

    public void setHandler(Handler handler) {
        dataHandler = handler;
        Log.d("BTScanner", "handler added");
    }

    public void connect() {
        thread = new BluetoothScannerThread();

        if (adapter.isEnabled()) {
            thread.start();
        }
    }

    public boolean isConnected() {
        return (thread != null && socket != null && socket.isConnected());
    }

    public void disconnect() {
        try {
            if (socket != null) {
                socket.close();
                sendMessage(toastHandler, context.getString(R.string.bt_scanner_offline));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private class BluetoothScannerThread extends Thread {

        @Override
        public void run() {
            try {
                socket = device.createRfcommSocketToServiceRecord(UUID.fromString(UUID_SPP));
                adapter.cancelDiscovery();
                socket.connect();

                if (socket.isConnected()) {
                    sendMessage(toastHandler, String.format(context.getString(R.string.bt_scanner_online), device.getName()));
                }

                BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                String data;
                while ((data = reader.readLine()) != null) {
                    sendMessage(dataHandler, data);
                }

            } catch (IOException e) {
                Log.d("BT", "Connection failed");
                sendMessage(toastHandler, context.getString(R.string.bt_scanner_offline));

            }
        }
    }

}

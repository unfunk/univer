package com.univer;

import android.util.Log;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class InventoryList extends LinkedHashMap<Integer, Product> {
    private HashMap<Integer, Integer> idIndex;
    private HashMap<String, Integer> barcodeIndex;
    private int current;

    public InventoryList() {
        super();
        idIndex = new HashMap<>();
        barcodeIndex = new HashMap<>();
        current = 0;
    }

    public InventoryList(int capacity) {
        super();
        idIndex = new HashMap<>(capacity);
        barcodeIndex = new HashMap<>(capacity);
        current = 0;
    }

    @Override
    public Product put(Integer key, Product value) {
        if (containsKey(key)) {
            value = get(key);
            value.setCount(value.count() + 1);
            return value;
        }

        idIndex.put(size(), key);
        barcodeIndex.put(value.barcode(), key);
        return super.put(key, value);
    }

    public void put(Product product) {
        put(product.id(), product);
    }

    public Integer getIdByBarcode(String barcode) {
        return barcodeIndex.get(barcode);
    }

    public Product get(String barcode) {
        return get(getIdByBarcode(barcode));
    }

    public int indexOfKey(Integer id) {
        if (containsKey(id)) {
            for (Entry<Integer, Integer> entry : idIndex.entrySet()) {
                if (entry.getValue().equals(id))
                    return entry.getKey();
            }
        }

        return -1;
    }

    public Product at(int position) {
        return get(keyAt(position));
    }

    public Integer keyAt(int position) {
        return idIndex.get(position);
    }

    public void setCurrent(int position) {
        current = position;
    }

    public int getCurrent() {
        return current;
    }
}

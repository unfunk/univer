package com.univer;

import android.app.Activity;
import android.content.Intent;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class CameraActivity extends Activity implements
        SurfaceHolder.Callback,
        View.OnClickListener,
        Camera.PictureCallback,
        Camera.PreviewCallback,
        Camera.AutoFocusCallback
{

    private final static String TAG = "CameraActivity";

    private Camera camera;
    private SurfaceHolder surfaceHolder;
    private SurfaceView surfaceView;
    private ImageButton buttonShot;
    private LinearLayout layoutCamera;

    private Product product;
    private File photoFile;
    private String photoPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_camera);

        layoutCamera = (LinearLayout) findViewById(R.id.layoutCamera);

        surfaceView = (SurfaceView) findViewById(R.id.surfaceView);
        surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(this);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        buttonShot = (ImageButton) findViewById(R.id.buttonShot);
        buttonShot.setOnClickListener(this);

        product = new Product();
        product.setId(getIntent().getIntExtra("id", 0));
        product.setName(getIntent().getStringExtra("name"));

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();

        camera = Camera.open();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (camera != null) {
            camera.setPreviewCallback(null);
            camera.stopPreview();
            camera.release();
            camera = null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_camera, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onClick(View view) {
        Log.d(TAG, "onClick");
        if (view == buttonShot) {
            camera.autoFocus(this);
        } else if (view == surfaceView) {
            Log.d(TAG, "Click surfaceView");
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.d(TAG, "surfaceChanged");

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.d(TAG, "surfaceCreated");

        try {
            camera.setPreviewDisplay(holder);
            camera.setPreviewCallback(this);
        } catch (IOException e) {
            e.printStackTrace();
        }

        camera.setDisplayOrientation(0);

        Camera.Parameters parameters = camera.getParameters();
        List<Camera.Size> sizes = parameters.getSupportedPreviewSizes();

        for (Camera.Size size : sizes) {
            Log.d(TAG, "Size: "+ size.width + "x"+ size.height);
        }

        parameters.setPictureSize(1024, 768);
        parameters.setPreviewSize(640, 480);
        parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);

        camera.setParameters(parameters);

        Camera.Size previewSize = parameters.getPreviewSize();
        Camera.Size pictureSize = parameters.getPictureSize();

        /*
        Log.d(TAG, "pictureSize: "+ pictureSize.width + "x"+ pictureSize.height);
        Log.d(TAG, "previewSize: "+ previewSize.width + "x"+ previewSize.height);
        Log.d(TAG, "surfaceSize: "+ surfaceView.getWidth() + "x"+ surfaceView.getHeight());

        float aspect = (float) pictureSize.width / pictureSize.height;

        int previewSurfaceWidth = surfaceView.getWidth();
        int previewSurfaceHeight = surfaceView.getHeight();

        ViewGroup.LayoutParams lp = surfaceView.getLayoutParams();
        lp.height = previewSurfaceHeight;
        lp.width = (int) (previewSurfaceHeight * aspect);

        surfaceView.setLayoutParams(lp);

        Log.d(TAG, "surfaceSize: "+ surfaceView.getWidth() + "x"+ surfaceView.getHeight());
        */
        camera.startPreview();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    @Override
    public void onPictureTaken(byte[] paramArrayOfByte, Camera paramCamera) {
        Log.d(TAG, "onPictureTaken");

        try {
            photoFile = createImageFile();
            photoPath = photoFile.getAbsolutePath();

            FileOutputStream os = new FileOutputStream(photoFile);
            os.write(paramArrayOfByte);
            os.close();

            Intent intent = new Intent();
            intent.putExtra("photoPath", photoPath);
            setResult(Activity.RESULT_OK, intent);
            finish();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onAutoFocus(boolean paramBoolean, Camera paramCamera) {
        Log.d(TAG, "onAutoFocus");
        if (paramBoolean) {
            paramCamera.takePicture(null, null, null, this);
        }
    }

    @Override
    public void onPreviewFrame(byte[] paramArrayOfByte, Camera paramCamera) {}


    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = product.id() + "_" + timeStamp;
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        return image;
    }
}

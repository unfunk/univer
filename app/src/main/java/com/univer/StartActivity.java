package com.univer;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.JsonReader;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Map;

public class StartActivity extends Activity implements
    AdapterView.OnItemClickListener,
    AdapterView.OnItemLongClickListener
{

    private final static String TAG = "StartAcivity";

    private InventoryStorage db;
    private ListView listView;
    private ArrayList<Document> documents;
    private ProgressDialog dialogProgress;
    private String serverUrl;

    private ActionMode actionMode;

    class RequestTask extends AsyncTask<String, String, Void> {

        @Override
        protected Void doInBackground(String... params) {
            Log.d("RequestTask", "doInBackground");

            try {
                DefaultHttpClient hc = new DefaultHttpClient();
                HttpGet http = new HttpGet(serverUrl + params[0]);
                HttpResponse response = hc.execute(http);
                if (response.getStatusLine().getStatusCode() != 200) {
                    Log.d("RequestTask", "Http error "+ response.getStatusLine().getStatusCode());
                }

                /*
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));

                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        for (String value : line.split("\t")) {
                            Log.d("CSV", value);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                */
                db.beginTransaction();
                JsonReader reader = new JsonReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                try {
                    reader.beginObject();
                    reader.nextName();
                    int count = reader.nextInt();
                    Log.d("JSON", "Count "+ count);
                    dialogProgress.setMax(count);
                    reader.nextName();
                    reader.beginArray();

                    count = 1;
                    while (reader.hasNext()) {
                        reader.beginObject();

                        Product product = new Product();
                        while (reader.hasNext()) {
                            String name = reader.nextName();
                            switch (name) {
                                case Product.PRODUCT_ID:
                                    product.setId(reader.nextInt());
                                    break;
                                case Product.PRODUCT_NAME:
                                    product.setName(reader.nextString());
                                    break;
                                case Product.PRODUCT_STOCK:
                                    product.setStock(reader.nextInt());
                                    break;
                                case Product.PRODUCT_PRICE:
                                    product.setPrice(reader.nextDouble());
                                    break;
                                case Product.PRODUCT_BARCODE:
                                    product.setBarcode(reader.nextString());
                                    break;
                                case Product.PRODUCT_ARTICLE:
                                    product.setArticle(reader.nextString());
                                    break;
                            }
                        }
                        product.setCount(1);
                        reader.endObject();

                        db.saveProduct(product);
                        dialogProgress.setProgress(count++);
                    }
                    reader.endArray();
                    reader.endObject();
                    db.setTransactionSuccessful();
                } finally {
                    reader.close();
                    db.endTransaction();
                }

            } catch (Exception e) {
                Log.d("UpdateProducts", "Error:" + e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            dialogProgress.dismiss();
            Log.d("UpdateProducts", "onPostExecute:");
            super.onPostExecute(result);
        }

        @Override
        protected void onPreExecute() {
            dialogProgress = new ProgressDialog(StartActivity.this);
            dialogProgress.setMessage(StartActivity.this.getString(R.string.title_loading_products));
            dialogProgress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            dialogProgress.setProgress(0);
            dialogProgress.setCancelable(true);
            dialogProgress.show();
            super.onPreExecute();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        db = new InventoryStorage(this);

        documents = db.getDocuments(true);
        serverUrl = "http://" + db.getSettingsValue("server") + ":58888/";
        Log.d(TAG, serverUrl);

        listView = (ListView) findViewById(R.id.listViewDocuments);
        listView.setAdapter(new DocumentAdapter(this, documents));

        listView.setOnItemClickListener(this);
        listView.setOnItemLongClickListener(this);
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume()");
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        db.close();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_start, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_new:
                onSelectCreateInventory();
                break;

            case R.id.action_update:
                onSelectUpdate();
                break;

            case R.id.action_settings:
                onSelectSettings();
        }

        return super.onOptionsItemSelected(item);
    }

    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Document document = documents.get(position);
        openDocument(document);
    }

    protected void openDocument(Document document) {
        Intent intent = new Intent(StartActivity.this, InventoryActivity.class);
        intent.putExtra("documentId", document.id());
        startActivity(intent);
        overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out);
    }

    public boolean onSelectUpdate() {
        new RequestTask().execute("inventory/load/");
        return false;
    }

    public boolean onSelectSettings() {
        Intent intent = new Intent(StartActivity.this, SettingsActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out);
        return false;
    }


    public void onSelectCreateInventory() {
        Document document = db.createDocument();
        if (document == null) {
            Toast.makeText(this, getString(R.string.error_new_document), Toast.LENGTH_SHORT).show();
            return;
        }
        documents.add(0, document);
        openDocument(document);
        ((BaseAdapter) listView.getAdapter()).notifyDataSetChanged();
    }

    private void removeDocument(long id) {
        Log.d(TAG, "removeDocument:");
    }

    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        if (actionMode != null)
            return false;
        actionMode = startActionMode(actionModeCallback);
        return true;
    }

    private ActionMode.Callback actionModeCallback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.menu_start_context, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_delete:
                    removeDocument(0);
                    mode.finish();
                    return true;
            }
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            actionMode = null;
        }
    };

}


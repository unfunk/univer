package com.univer;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.DatabaseUtils;
import android.util.Log;
import android.util.SparseArray;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class InventoryStorage {

    private DBHelper dbHelper;
    private SQLiteDatabase db;

    public InventoryStorage(Context context) {
        dbHelper = new DBHelper(context);
        db = dbHelper.getWritableDatabase();
    }

    public void close() {
        if (db != null) {
            db.close();
        }
    }

    public void beginTransaction() {
        db.beginTransaction();
    }

    public void setTransactionSuccessful() {
        db.setTransactionSuccessful();
    }

    public void endTransaction() {
        db.endTransaction();
    }

    public void saveProduct(Product product) {
        saveProduct(
            product.id(),
            product.name(),
            product.article(),
            product.barcode(),
            product.stock(),
            product.price()
        );
    }

    public void saveProduct(int id, String name, String article, String barcode, int stock, double price) {
        ContentValues values = new ContentValues(6);
        values.put("id", id);
        values.put("name", name);
        values.put("article", article);
        values.put("barcode", barcode);
        values.put("stock", stock);
        values.put("price", price);
        db.replace(DBHelper.TABLE_PRODUCT, null, values);
    }

    public Product getProduct(int id) {
        Cursor cursor = db.query(DBHelper.TABLE_PRODUCT, null, "id="+id, null, null, null, null);

        if (cursor == null) {
            return null;
        }

        if (cursor.moveToFirst()) {
            Product product = new Product(
                    id,
                    cursor.getString(1),
                    cursor.getString(2),
                    0,
                    cursor.getInt(4),
                    cursor.getDouble(5),
                    cursor.getString(3)
            );
            cursor.close();
            return product;
        }

        return null;
    }

    public Product getProduct(String barcode) {
        Cursor cursor = db.query(DBHelper.TABLE_PRODUCT, null, "barcode = ?", new String[] { barcode }, null, null, null);

        if (cursor == null) {
            return null;
        }

        if (cursor.moveToFirst()) {
            Product product = new Product(
                    cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    1,
                    cursor.getInt(4),
                    cursor.getDouble(5),
                    barcode
            );
            cursor.close();
            return product;
        }

        return null;
    }

    public int getId(String barcode) {
        Cursor cursor = db.query(DBHelper.TABLE_PRODUCT, new String[] {"id"},  "barcode = ?", new String[] { barcode }, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            int id = cursor.getInt(0);
            cursor.close();
            return id;
        }

        return 0;
    }

    public String getBarcode(int id) {
        Cursor cursor = db.query(DBHelper.TABLE_PRODUCT, new String[] {"barcode"}, "id="+id, null, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            String barcode = cursor.getString(0);
            cursor.close();
            return barcode;
        }

        return "";
    }

    public long loadProducts(ProductsDatabase products) {
        Cursor cursor = db.query(DBHelper.TABLE_PRODUCT, null, null, null, null, null, null);

        if (cursor == null) {
            return 0;
        }

        if (cursor.moveToFirst()) {
            do {
                Product product = new Product(
                        cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        0,
                        cursor.getInt(4),
                        cursor.getDouble(5),
                        cursor.getString(3)
                );
                products.put(product.id(), product);
            } while (cursor.moveToNext());
        }

        if (!cursor.isClosed()) {
            cursor.close();
        }

        return products.size();
    }

    public void saveProductInventory(Document document, Product product, int position) {
        ContentValues values = new ContentValues(4);
        values.put("document", document.id());
        values.put("product", product.id());
        values.put("count", product.count());
        values.put("position", position);

        db.replace(DBHelper.TABLE_INVENTORY, null, values);
    }

    public Document createDocument() {
        ContentValues values = new ContentValues(1);
        SimpleDateFormat dateFormat = new SimpleDateFormat(DBHelper.DATE_FORMAT);
        Date date = new Date();
        values.put("date", dateFormat.format(date));
        long id = db.insert(DBHelper.TABLE_DOCUMENT, null, values);

        if (id != -1) {
            return new Document(id, date);
        }

        return null;
    }

    public long getDocumentsCount() {
        return DatabaseUtils.queryNumEntries(db, DBHelper.TABLE_DOCUMENT);
    }

    public ArrayList<Document> getDocuments(boolean reverse) {
        Cursor cursor = db.query(DBHelper.TABLE_DOCUMENT, null, null, null, null, null, "date "+ (reverse ? "DESC" : "ASC"));

        if (cursor == null) {
            return null;
        }

        ArrayList<Document> documents = new ArrayList<>(cursor.getCount());
        if (cursor.moveToFirst()) {
            do {
                Document document = new Document(cursor.getLong(0), cursor.getString(1), cursor.getLong(2));
                documents.add(document);
            } while (cursor.moveToNext());
        }

        if (!cursor.isClosed()) {
            cursor.close();
        }

        return documents;
    }

    public Document getDocument(long id) {
        Cursor cursor = db.query(DBHelper.TABLE_DOCUMENT, new String[] {"date", "external_id"},  "id="+ id, null, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            Document document = new Document(id, cursor.getString(0), cursor.getLong(1));
            cursor.close();
            return document;
        }

        return null;
    }

    public InventoryList getInventoryProducts(Document document) {
        Cursor cursor = db.rawQuery(
            "SELECT " +
                "product.id," +
                "product.name," +
                "product.article," +
                "inventory.count," +
                "product.stock," +
                "product.price," +
                "product.barcode " +
            "FROM "+ DBHelper.TABLE_INVENTORY +" AS inventory " +
            "LEFT JOIN "+ DBHelper.TABLE_PRODUCT +" AS product ON inventory.product = product.id " +
            "WHERE document=? ORDER BY inventory.position",
            new String[] { String.valueOf(document.id()) }
        );

        if (cursor == null) {
            return null;
        }

        InventoryList products = new InventoryList(cursor.getCount());
        if (cursor.moveToFirst()) {
            do {
                products.put(new Product(
                    cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getInt(3),
                    cursor.getInt(4),
                    cursor.getDouble(5),
                    cursor.getString(6)
                ));
            } while (cursor.moveToNext());
        }

        cursor.close();

        return products;
    }

    public String getSettingsValue(String name) {
        Cursor cursor = db.query(DBHelper.TABLE_SETTINGS, new String[] {"value"}, "name=?", new String[] {name}, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            String value = cursor.getString(0);
            return value;
        }

        return null;
    }

    public void setSettingsValue(String name, String value) {
        ContentValues values = new ContentValues(2);
        values.put("name", name);
        values.put("value", value);
        long r = db.replace(DBHelper.TABLE_SETTINGS, null, values);
        Log.d("DB", "replace: "+ r);
    }

}
